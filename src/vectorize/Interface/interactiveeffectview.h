#ifndef INTERACTIVEEFFECTVIEW_H
#define INTERACTIVEEFFECTVIEW_H

#include <QDialog>
#include "imageinterface.h"
#include "Model/image.h"


QT_BEGIN_NAMESPACE
class QVBoxLayout;
class QPushButton;
QT_END_NAMESPACE


/*!
 * \class InteractiveEffectView
 * \brief Vue permettant d'afficher les options d'un effet et modifier interactivement une image
 */
class InteractiveEffectView : public QDialog
{
    Q_OBJECT
public:
    InteractiveEffectView(ImageInterface * data_img, QWidget *parent = NULL);



public slots:

    /*!
     * \brief Slot appellé lors du changement des parametres du filtre
     */
    void previewModification();

    /*!
     * \brief Slot appellé pour terminer la modification et appliquer le filtre
     */
    void applyModification();

    void reject();

protected:

    /*!
     * \brief Function called by prewiedModification and applyModification before applying the filter
     */
    virtual void updateFilter() = 0;

    ImageInterface * img_view;
    Image * img_base;
    Filter * filtre;
    QVBoxLayout * settings_layout;
    QPushButton * apply_btn;

private:
    bool apply;

};


#endif // INTERACTIVEEFFECTVIEW_H
