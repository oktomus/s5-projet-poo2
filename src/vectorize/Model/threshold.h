#ifndef THRESHOLD_H
#define THRESHOLD_H

/*!
  * \file threshold.h
  * \brief Filtre de Threshold
  * \author Kevin.M
  * \version 0.1
  */

#include "filter.h"


/*!
 * \class Threshold
 * \brief Filtre de Threshold (seuil)
 */
class Threshold : public Filter
{
public:

    /*!
     * \brief Constructeur par défaut
     * \param v     Valeur du seil (pourcentage)
     */
    Threshold(int v): Filter(){
        this->value = v;
    };

    void compute(Image * img) const;

    void setValue(int v){
        this->value = v;
    }

private:

    /*!
     * \brief Une valeur entre 0 et 100
     * Pourcentage de seuil
     */
    int value;

};





#endif // THRESHOLD_H
