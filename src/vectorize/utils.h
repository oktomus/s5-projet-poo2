#ifndef UTILS_H
#define UTILS_H

/*!
  * \file utils.h
  * \brief Fonctions utiles
  * \author Kevin.M
  * \version 0.1
  */

#include <sys/stat.h>
#include <unistd.h>
#include <string>

using namespace std;

/*!
 * \brief Verifie si un fichier existe
 * \param name Chemin du fichier
 * \return true si le fichier existe, false sinon
 */
inline bool file_exists (string* name) {
  struct stat buffer;
  return (stat (name->c_str(), &buffer) == 0);
}

#endif // UTILS_H
