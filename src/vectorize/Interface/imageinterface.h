#ifndef IMAGEINTERFACE_H
#define IMAGEINTERFACE_H

/*!
* \file imageinterface.h
* \brief Vue d'un modèle image
* \author Kevin.M
* \version 0.1
*/

#include <QWidget>
#include "Model/image.h"

QT_BEGIN_NAMESPACE
class QLabel;
class QScrollArea;
class QKeyEvent;
QT_END_NAMESPACE



/*!
 * \class ImageInterface
 * \brief Widget permettant de visualiser une image
 */
class ImageInterface : public QWidget
{
    Q_OBJECT
public:

    /*!
     * \brief Constructeur par défaut
     * \param parent
     */
    explicit ImageInterface(QWidget *parent = 0);

    /*!
     * \brief Definis l'image de la vue
     * L'ancienne image est supprimée
     * \param img Un pointeur vers l'image
     */
    void setImage(Image * img);

    /*!
     * \brief Met a jour la vue avec la nouvelle image
     */
    void updateView();

    /*!
     * \brief Accesseur vers l'image
     * \return Un pointeur vers l'image
     */
    Image * getImage();

    void keyReleaseEvent(QKeyEvent *event);


private:

    /*!
     * \brief L'image courrante de l'application
     */
    Image *image;

    /*!
     * \brief Widget contenant l'image
     */
    QLabel *imageContainer;

    QScrollArea *scrollArea;

    double scale_factor;

    void scaleImage(double factor);

signals:

public slots:

    void zoomIn();
    void zoomOut();
};


#endif // IMAGEINTERFACE_H
