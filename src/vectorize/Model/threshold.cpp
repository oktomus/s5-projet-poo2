#include "threshold.h"
#include "image.h"
#include <iostream>

void Threshold::compute(Image * img) const{


    int seuil = 255 * ((double) value / 100.0);

    if(seuil > 0){

        int width = img->width();
        int height = img->height();

        QRgb * pixels = (QRgb *) img->bits();

        //#pragma omp parallel for
        for (int index = 0; index < width * height; ++index){
            QRgb *pixel_ptr = pixels + index;

            if(qRed(*pixel_ptr) < seuil){
                *pixel_ptr = qRgb(0, 0, 0);
            }
        }

    }

}

