#ifndef PREWITT_H
#define PREWITT_H

/*!
  * \file prewitt.h
  * \brief Filtre de Prewitt
  * \author Kevin.M
  * \version 0.1
  */

#include "sobel.h"


/*!
 * \class Prewitt
 * \brief Filtre de Prewitt
 */
class Prewitt : public Sobel
{
public:
    Prewitt() : Sobel(){
        double mx[3][3]{
            {-1,    0,      1},
            {-1,    0,      1},
            {-1,    0,      1}
        };
        double my[3][3]{
            {-1,    -1,     -1},
            {0,     0,      0},
            {1,     1,      1}
        };
        memcpy(this->matrice_x, mx, 3 * 3 * sizeof(double));
        memcpy(this->matrice_y, my, 3 * 3 * sizeof(double));
    }
};



#endif // PREWITT_H
