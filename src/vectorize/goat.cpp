#include "goat.h"

#include <QtWidgets>
#include "Model/image.h"
#include <string>
#include <iostream>
#include "Model/filter.h"
#include "Model/fastblur.h"
#include "Model/threshold.h"
#include "Model/sobel.h"
#include "Model/prewitt.h"
#include "Interface/imageinterface.h"
#include "Interface/interactiveeffectview.h"
#include "Interface/interactivefastblurview.h"
#include "Interface/interactivethresholdview.h"

using namespace std;

/*!
 * \brief Goat::Goat
 *
 * Creer une application
 * Appelle toutes les methodes de creation de l'interface et des modeles
 * Met en place les interactions
 *
 * \param parent
 */
Goat::Goat(QWidget *parent) :
    QMainWindow(parent){

    signalMapper = new QSignalMapper(this);

    createMenu();
    createStatusBar();
    createContent();

    content = new QWidget();

    QHBoxLayout* mainLayout = new QHBoxLayout();

    mainLayout->setMenuBar(menuBar);
    mainLayout->addWidget(imageContainer);


    content->setLayout(mainLayout);
    setCentralWidget(content);

}

/*!
 * \brief Goat::createMenu
 *
 * Creer les menu et sous menu ainsi que les actions associées
 * Relis les methodes aux actions
 */
void Goat::createMenu(){
    menuBar = new QMenuBar();

    imageMenu = new QMenu("Image", this);

    menu_a_loadImage = imageMenu->addAction("Load");
    menu_a_loadImage->setStatusTip("Load a new image");
    menu_a_saveImage = imageMenu->addAction("Save");
    menu_a_saveImage->setStatusTip("Save the current image");
    menu_a_saveImage->setEnabled(false);

    connect(menu_a_loadImage, SIGNAL(triggered()), this, SLOT(loadImage()));
    connect(menu_a_saveImage, SIGNAL(triggered()), this, SLOT(saveImage()));

    effectMenu = new QMenu("Effect", this);

    menu_a_fastBlur = effectMenu->addAction("Fast blur");
    menu_a_fastBlur->setStatusTip("Add a fast blur to the image");
    menu_a_fastBlur->setEnabled(false);
    effectMenu->addSeparator();
    menu_a_sobel = effectMenu->addAction("Edge detection : Sobel filter");
    menu_a_sobel->setStatusTip("Add a sobel filter to the image");
    menu_a_sobel->setEnabled(false);
    menu_a_prewitt = effectMenu->addAction("Edge detection : Prewitt filter");
    menu_a_prewitt->setStatusTip("Add a prewitt filter to the image");
    menu_a_prewitt->setEnabled(false);
    effectMenu->addSeparator();
    menu_a_threshold = effectMenu->addAction("Threshold");
    menu_a_threshold->setStatusTip("Threshold the image with a given value");
    menu_a_threshold->setEnabled(false);

    //connect(menu_a_fastBlur, SIGNAL(triggered()), this, SLOT(startInteractiveEffect(FAST_BLUR)));

    // Mapping des effets
    connect(menu_a_fastBlur, SIGNAL(triggered()), signalMapper, SLOT(map()));
    signalMapper->setMapping(menu_a_fastBlur, 1);

    connect(menu_a_sobel, SIGNAL(triggered()), signalMapper, SLOT(map()));
    signalMapper->setMapping(menu_a_sobel, 201);

    connect(menu_a_prewitt, SIGNAL(triggered()), signalMapper, SLOT(map()));
    signalMapper->setMapping(menu_a_prewitt, 202);

    connect(menu_a_threshold, SIGNAL(triggered()), signalMapper, SLOT(map()));
    signalMapper->setMapping(menu_a_threshold, 2);

    connect(signalMapper, SIGNAL(mapped(int)), this, SIGNAL(contextEffectCalled(int)));
    connect(this, SIGNAL(contextEffectCalled(int)), this, SLOT(addImageEffect(int)));

    menuBar->addMenu(imageMenu);
    menuBar->addMenu(effectMenu);
}

/*!
 * \brief Goat::createStatusBar
 */
void Goat::createStatusBar()
{
    statusBar()->showMessage("Ready");
}

/*!
 * \brief Goat::createContent
 */
void Goat::createContent(){
    imageContainer = new ImageInterface();
}

/*!
 * \brief Goat::enableInteraction
 *
 * Active les actions effets
 */
void Goat::enableInteraction(){
    menu_a_fastBlur->setEnabled(true);
    menu_a_sobel->setEnabled(true);
    menu_a_prewitt->setEnabled(true);
    menu_a_saveImage->setEnabled(true);
    menu_a_threshold->setEnabled(true);

}

void Goat::disableActions(){
    this->menuBar->setEnabled(false);
}

void Goat::enableActions(){
    this->menuBar->setEnabled(true);
    statusBar()->showMessage("");
}

/*!
 * \brief Goat::loadImage
 *
 * Verifie si le fichier courant a subis des modifications et propose son enregistrement
 * Ouvre un file dialog pour choisir une image à ouvrir
 * Affiche l'image dans l'application
 *
 */
void Goat::loadImage(){
    QString fileName = QFileDialog::getOpenFileName(this, "Open an image",QDir::homePath(),"Image (*.png *.jpeg *.jpg *.bmp *.xpm)");

    if (!fileName.isEmpty()) {
       Image imageBase(fileName.toStdString());

       Image * image;
       image = new Image(imageBase.convertToFormat(QImage::Format_RGB32));

       imageContainer->setImage(image);
       //imageContainer->setPixmap(QPixmap::fromImage(*image));

       this->enableInteraction();

       statusBar()->showMessage("Your image was correctyl loaded !");
    }else{
        statusBar()->showMessage("No file selected");
    }
}

/*!
 * \brief Goat::saveImage
 *
 * Ouvre un file dialog pour sauvegarder l'image courrante à la destination désirée
 *
 */
void Goat::saveImage(){
    QString fileName = QFileDialog::getSaveFileName(this, "Save the image", QDir::homePath(),"Image (*.png *.jpeg *.jpg *.bmp *.xpm)");
    bool saved = false;
    if (!fileName.isEmpty()) {
        saved = ((QImage *) this->imageContainer->getImage())->save(fileName.toStdString().c_str());

    }

    if(!saved) statusBar()->showMessage("Wrong filename");
    if(saved) statusBar()->showMessage("Your image was correctyl saved !");

}

/*!
 * \brief Application::addImageEffect
 * \param effect
 */
void Goat::addImageEffect(int effect){
    statusBar()->showMessage("Applying filter...");

    if(effect >= 200){ // Direct filter
        Filter *filter = 0;
        if( effect == 201 ){  // SOBEL
            filter = new Sobel();
        }else if( effect == 202 ){
            filter = new Prewitt();
        }
        if(filter != 0){
            imageContainer->getImage()->applyFilter(filter);
            imageContainer->updateView();
        }
    }else{ // Interactive filter
        InteractiveEffectView *filter = 0;
        if(effect == 1){
            filter = new InteractiveFastBlurView(imageContainer);
        }else if(effect == 2){
            filter = new InteractiveThresholdView(imageContainer);
        }
        if(filter != 0){
            this->disableActions();
            filter->show();
            connect(filter, SIGNAL(destroyed(QObject*)), this, SLOT(enableActions()));
        }


    }

}

/*!
 * \brief Application::~Application
 */
Goat::~Goat()
{
}
