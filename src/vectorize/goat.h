#ifndef GOAT_H
#define GOAT_H

/*!
  * \file goat.h
  * \brief Editeur d'image de base
  * \author Kevin.M
  * \version 0.1
  */

#include <QMainWindow>
#include "Model/image.h"
#include "Interface/imageinterface.h"

QT_BEGIN_NAMESPACE
class QAction;
class QDialogButtonBox;
class QGroupBox;
class QLabel;
class QLineEdit;
class QMenu;
class QMenuBar;
class QPushButton;
class QTextEdit;
class QSignalMapper;
QT_END_NAMESPACE


/*!
 * \class Goat
 * \brief Classe principale de l'application
 */
class Goat : public QMainWindow
{
    Q_OBJECT

public:
    /*!
     * \brief Constructeur
     *
     * Constructeur par defaut de l'application
     *
     * \param parent Le widget parent dans lequel mettre l'application (non nécessaire)
     */
    explicit Goat(QWidget *parent = 0);

    /*!
      * \brief Destructeur
      *
      */
    ~Goat();



signals:
    /*!
     * \brief Signal émit lorsque un boutton dans le menu effet est cliqué
     *
     * \param effect Effet à ajouter
     */
    void contextEffectCalled(int effect);


private:

    /*!
     * \brief Widget contenant toutes les sous interfaces
     */
    QWidget *content;

    /*!
     * \brief Menu haut
     */
    QMenuBar *menuBar;

    /*!
     * \brief Menu image
     */
    QMenu *imageMenu;

    /*!
     * \brief Action de chargement d'une image du menu image
     */
    QAction *menu_a_loadImage;

    /*!
     * \brief Action de sauvegarde d'une image du menu image
     */
    QAction *menu_a_saveImage;

    /*!
     * \brief Menu des effets
     */
    QMenu *effectMenu;

    /*!
     * \brief Action pour l'effet fast blur
     */
    QAction *menu_a_fastBlur;

    /*!
     * \brief Action pour l'effet sobel
     */
    QAction *menu_a_sobel;

    /*!
     * \brief Action pour l'effet prewitt
     */
    QAction *menu_a_prewitt;

    /*!
     * \brief Action pour l'effet threshold
     */
    QAction *menu_a_threshold;


    /*!
     * \brief Widget contenant l'image à modifier
     */
    ImageInterface *imageContainer;

    QSignalMapper *signalMapper;

    /*!
     * \brief Creer le menu des actions de l'application
     */
    void createMenu();

    /*!
     * \brief Creer la barre de status
     */
    void createStatusBar();

    /*!
     * \brief Creer le contenu graphique de l'application
     */
    void createContent();

    /*!
     * \brief Autorise les interactions avec l'interface
     */
    void enableInteraction();


public slots:
    /*!
     * \brief Autorise les modifications de l'image courrante de l'application
     */
    void enableActions();

    /*!
     * \brief Bloque les interactions avec l'interface
     */
    void disableActions();

private slots:



    /*!
     * \brief Slot appellé lorsque l'action de chargement d'une image est déclanchée
     *
     * Charge une image
     */
    void loadImage();

    /*!
     * \brief Slot appellé lorsque l'action de sauvegarde est déclanchée
     *
     * Sauvegarde l'image courrante
     */
    void saveImage();

    /*!
     * \brief Ajoute un effet à l'image
     * \param effect    l'effet à ajouter
     */
    void addImageEffect(int effect);


};

#endif // GOAT_H
