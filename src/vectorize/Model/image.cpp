#include "image.h"
#include "utils.h"
#include <string.h>
#include <iostream>
#include <QImage>
#include "filter.h"

using namespace std;

/*!
 * \brief Image::Image
 *
 * Appelle le constructeur parent QImage
 *
 * \param path
 */
Image::Image(string* path) : QImage(path->c_str())
{
}

/*!
 * \brief Image::applyFilter
 * \param f
 */
void Image::applyFilter(const Filter *f){
    f->compute(this);
    //this->swap(newImage);
}



