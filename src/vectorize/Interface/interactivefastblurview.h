#ifndef INTERACTIVEFASTBLURVIEW_H
#define INTERACTIVEFASTBLURVIEW_H

#include "interactiveeffectview.h"

#include "Model/fastblur.h"
#include <QSlider>
#include <QVBoxLayout>
#include <QtWidgets>

class InteractiveFastBlurView : public InteractiveEffectView
{
public:
    InteractiveFastBlurView(ImageInterface * data_img) : InteractiveEffectView(data_img){
        this->filtre = new FastBlur(0);

        this->size = new QSlider(Qt::Horizontal);
        QLabel *name = new QLabel("Blur size");
        this->settings_layout->addWidget(name);
        this->settings_layout->addWidget(this->size);
        this->size->setMinimum(0);
        this->size->setMaximum(10);

        connect(this->size, SIGNAL(valueChanged(int)), this, SLOT(previewModification()));
    }

private:
    QSlider * size;

protected:
    void updateFilter(){
        ((FastBlur *) this->filtre)->setSize(this->size->value());
    }
};


#endif // INTERACTIVEFASTBLURVIEW_H
