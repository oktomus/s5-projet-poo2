#include "imageinterface.h"
#include <QtWidgets>

#include "Model/image.h"



ImageInterface::ImageInterface(QWidget *parent) : QWidget(parent), scale_factor(1)
{
    this->image = NULL;

    QHBoxLayout* mainLayout = new QHBoxLayout();
    mainLayout->setMargin(0);

    imageContainer = new QLabel();
    imageContainer->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    imageContainer->setScaledContents(true);

    scrollArea = new QScrollArea();
    scrollArea->setBackgroundRole(QPalette::Dark);
    scrollArea->setWidget(imageContainer);
    scrollArea->setVisible(false);

    mainLayout->addWidget(scrollArea);


    this->setLayout(mainLayout);
    resize(QGuiApplication::primaryScreen()->availableSize() * 3 / 5);
}

void ImageInterface::keyReleaseEvent(QKeyEvent *event){
    if(event->key() == Qt::Key_Plus){
        zoomIn();
    }else if(event->key() == Qt::Key_Minus){
        zoomOut();
    }
}

void ImageInterface::setImage(Image *img){
    if(this->image != img){
        free(this->image);
    }
    this->image = img;
    this->updateView();
    scale_factor = 1.0;
    scrollArea->setVisible(true);
    imageContainer->adjustSize();
}

void ImageInterface::updateView(){
    imageContainer->setPixmap(QPixmap::fromImage(*this->image));
}

Image * ImageInterface::getImage(){
    return this->image;
}

void ImageInterface::scaleImage(double factor){
    scale_factor *= factor;
    imageContainer->resize(scale_factor * imageContainer->pixmap()->size());

    scrollArea->horizontalScrollBar()->setValue(int(factor * scrollArea->horizontalScrollBar()->value()
                                + ((factor - 1) * scrollArea->horizontalScrollBar()->pageStep()/2)));

    scrollArea->verticalScrollBar()->setValue(int(factor * scrollArea->verticalScrollBar()->value()
                                + ((factor - 1) * scrollArea->verticalScrollBar()->pageStep()/2)));

}

void ImageInterface::zoomIn(){
    this->scaleImage(1.1);
}

void ImageInterface::zoomOut(){
    this->scaleImage(0.9091);
}



