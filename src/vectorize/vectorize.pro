#-------------------------------------------------
#
# Project created by QtCreator 2016-11-06T11:21:47
#
#-------------------------------------------------

QT       += core gui

QMAKE_CXXFLAGS += -fopenmp
LIBS += -fopenmp

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = vectorize
TEMPLATE = app


SOURCES += main.cpp\
    Model/image.cpp \
    Model/sobel.cpp \
    Model/fastblur.cpp \
    Model/threshold.cpp \
    Interface/imageinterface.cpp \
    goat.cpp \
    Interface/interactiveeffectview.cpp

HEADERS  += \
    utils.h \
    Model/image.h \
    Model/filter.h \
    Model/fastblur.h \
    Model/sobel.h \
    Model/threshold.h \
    Interface/imageinterface.h \
    goat.h \
    Model/prewitt.h \
    Interface/interactiveeffectview.h \
    Interface/interactivefastblurview.h \
    Interface/interactivethresholdview.h

FORMS    +=
