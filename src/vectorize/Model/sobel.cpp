#include "sobel.h"
#include "image.h"
#include <iostream>
#include <typeinfo>


void Sobel::compute(Image * img) const{

    /*std::cout << "Compute filter " <<  typeid(this).name() << std::endl;

    std::cout << "Matrice X " << std::endl;
    for (int pos_x = 0; pos_x < 3; ++pos_x) {
        for (int pos_y = 0; pos_y < 3; ++pos_y) {
            std::cout << (int) this->matrice_x[pos_x][pos_y] << "  ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
    std::cout << "Matrice Y " << std::endl;
    for (int pos_x = 0; pos_x < 3; ++pos_x) {
        for (int pos_y = 0; pos_y < 3; ++pos_y) {
            std::cout << (int) this->matrice_x[pos_x][pos_y] << "  ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;

    */
    int width = img->width();
    int height = img->height();

    std::vector<QRgb> original_pixels;
    original_pixels.resize(width * height);
    memmove(original_pixels.data(), img->bits(), height * width * sizeof(QRgb));

    QRgb * pixels = (QRgb *) img->bits();
    QRgb * start = pixels;

    #pragma omp parallel for
    for (int index = 0; index < width * height; ++index){ // On ne calcule pas les 1 px autour
        //int index = pixels - start;
        int x = index % width;
        int y = index / width;
        QRgb *pixel_ptr = start + index;

        if(x == 0 || x == width-1 || y == 0 || y == height-1){
            *pixel_ptr = qRgb(0, 0, 0);
        }else{

            int mov_x = 0;
            int mov_y = 0;
            //std::cout << "Pixel at " << x << ", " << y << " index " << index << " is red : " << qRed(*pixels) << std::endl;
            //*ptr = qRgb(qRed(*ptr), qRed(*ptr), qRed(*ptr));

            int index_offset;

            for (int pos_x = 0; pos_x < 3; ++pos_x) {
                for (int pos_y = 0; pos_y < 3; ++pos_y) {
                    index_offset = (x + pos_x - 1) + ((y + pos_y - 1) * width);
                    //std::cout << "pos x " << pos_x << " pos y " << pos_y << " Index off is " << index_offset << std::endl;
                    mov_x += qRed(original_pixels[index_offset]) * matrice_x[pos_x][pos_y];
                    mov_y += qRed(original_pixels[index_offset]) * matrice_y[pos_x][pos_y];
                    //std::cout << "  Matrice x is " << (int) matrice_x[pos_x][pos_y] << std::endl;
                    //std::cout << "  Matrice y is " << (int) matrice_y[pos_x][pos_y] << std::endl;
                    //std::cout << "  Red is " << qRed(original_pixels[index_offset]) << std::endl;
                }
            }
            //std::cout << "Mag x is " << mov_x << std::endl;
            int value = sqrt( pow(mov_x, 2) + pow(mov_y, 2) );
            //std::cout << "Value is " << value << std::endl;
            *pixel_ptr = qRgb(value, value, value);

            //break;

        }

    }

}


