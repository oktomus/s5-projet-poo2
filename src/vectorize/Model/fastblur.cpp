#include "fastblur.h"
#include "image.h"
#include <iostream>
#include <QImage>
#include <vector>


/*!
 * \brief FastBlur::compute
 *
 * Pour chaque pixel
 * Recupere les {size} pixels alentours dans chaque direction
 *
 * Initialisation d'un tableau de pixels
 *
 * Pour x de 0 a width
 *  Pour y de 0 a width
 *      Initialiser int r g b
 *      Ajout de la couleur dans le tableau de pixel
 *      Pour x2 de x-size/2 à y + size/2
 *          Pour y2 de xy-size/2 à y + size / 2
 *              Si x2 < x || y2 < y alors
 *                  Recuperer couleur depuis le tableau
 *              Sinon
 *                  Recupere couleur;
 *              r = r + couleur.r
 *              etc..
 *      couleur finale = rgb / size²
 *
 *
 *
 *
 * \param img
 */
void FastBlur::compute(Image * img) const{

    if(size > 0){

        int width = img->width();
        int height = img->height();

        std::vector<QRgb> original_pixels;
        //std::cout << "Res is " << img->width() * img->height() << std::endl;
        original_pixels.resize(width * height);

        memmove(original_pixels.data(), img->bits(), height * width * sizeof(QRgb));

        /*std::for_each(pixels.begin(), pixels.end(), [](QRgb& c)
            { c = qRgb(qRed(c), 0, 0); }
        );*/

        QRgb * pixels = (QRgb *) img->bits();
        QRgb * start = pixels;

        int divisor = pow(size * 2 + 1, 2);

        if(img->isGrayscale()){

            #pragma omp parallel for
            for (int index = 0; index < width * height; ++index){

                int value = 0;

                //int index = pixels - start;
                int x = index % width;
                int y = index / width;

                //std::cout << "Pixel at " << x << ", " << y << " index " << index << " is red : " << qRed(*pixels) << std::endl;
                //*ptr = qRgb(qRed(*ptr), qRed(*ptr), qRed(*ptr));


                for (int offset_x = x - size; offset_x <= x + size; ++offset_x) {
                    for (int offset_y = y - size; offset_y <= y + size; ++offset_y) {
                        if(offset_x >= 0 && offset_x < width && offset_y >= 0 && offset_y < height){
                            value += qRed(original_pixels[(offset_y * width) + offset_x]);
                        }
                    }
                }

                QRgb *pixel_ptr = start + index;

                value /= divisor;
                *pixel_ptr = qRgb(value, value, value);


                //if (index == 901) break;
            }

        }else{
            #pragma omp parallel for
            for (int index = 0; index < width * height; ++index){

                int r_value = 0;
                int g_value = 0;
                int b_value = 0;

                int x = index % width;
                int y = index / width;


                for (int offset_x = x - size; offset_x <= x + size; ++offset_x) {
                    for (int offset_y = y - size; offset_y <= y + size; ++offset_y) {
                        if(offset_x >= 0 && offset_x < width && offset_y >= 0 && offset_y < height){
                            r_value += qRed(original_pixels[(offset_y * width) + offset_x]);
                            g_value += qGreen(original_pixels[(offset_y * width) + offset_x]);
                            b_value += qBlue(original_pixels[(offset_y * width) + offset_x]);
                        }
                    }
                }

                QRgb *pixel_ptr = start + index;

                r_value /= divisor;
                g_value /= divisor;
                b_value /= divisor;
                *pixel_ptr = qRgb(r_value, g_value, b_value);


            }
        }

    }

}

