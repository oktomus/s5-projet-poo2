#include "interactiveeffectview.h"
#include <iostream>

#include <QVBoxLayout>
#include <QHBoxLayout>

#include <QPushButton>
#include <QSpacerItem>
#include <QCloseEvent>

InteractiveEffectView::InteractiveEffectView(ImageInterface * data_img, QWidget *parent) : QDialog(parent), img_view(data_img)
{

    this->setAttribute(Qt::WA_DeleteOnClose);
    apply = false;
    Image * current = data_img->getImage();
    this->img_base = new Image(*current);
    this->filtre = 0;

    QVBoxLayout *mainLayout = new QVBoxLayout();
    QHBoxLayout *buttons_layout = new QHBoxLayout();
    settings_layout = new QVBoxLayout();

    apply_btn = new QPushButton("Apply");
    QPushButton * cancel_btn = new QPushButton("Cancel");



    QSpacerItem *spacer = new QSpacerItem(0,0,QSizePolicy::Expanding);
    buttons_layout->addSpacerItem(spacer);
    buttons_layout->addWidget(cancel_btn);
    buttons_layout->addWidget(apply_btn);

    this->setLayout(mainLayout);
    mainLayout->addLayout(settings_layout);
    mainLayout->addLayout(buttons_layout);

    this->setWindowFlags(Qt::WindowStaysOnTopHint);

    connect(apply_btn, SIGNAL(clicked()), this, SLOT(applyModification()));
    connect(cancel_btn, SIGNAL(clicked(bool)), this, SLOT(reject()));

}

void InteractiveEffectView::reject(){
    QDialog::reject();
    if(!apply){
        Image * current = this->img_view->getImage();
        memcpy(current->bits(), this->img_base->bits(), current->height() * current->width() * sizeof(QRgb));
        this->img_view->updateView();
    }
    QDialog::close();
}

void InteractiveEffectView::previewModification(){
    this->updateFilter();
    Image * current = this->img_view->getImage();
    memcpy(current->bits(), this->img_base->bits(), current->height() * current->width() * sizeof(QRgb));
    current->applyFilter(this->filtre);
    this->img_view->updateView();
}

void InteractiveEffectView::applyModification(){
    this->previewModification();
    apply = true;
    this->reject();
}


