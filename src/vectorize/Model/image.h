#ifndef IMAGE_H
#define IMAGE_H

/*!
  * \file image.h
  * \brief Model permettant de manipuler une image
  * \author Kevin.M
  * \version 0.1
  */

#include <exception>
#include <vector>
#include <string>
#include <QImage>


class Filter;

/*!
 * \class ImageNoFile
 * \brief Le chemin ne correspont pas à un fichier
 */
class ImageNoFile : public std::exception
{
  virtual const char* what() const throw()
  {
    return "There is no valid file at the given path";
  }
};

/*!
 * \class ImageExtractImpossible
 * \brief Impossible de récupérer des données du fichier
 */
class ImageExtractImpossible : public std::exception
{
  virtual const char* what() const throw()
  {
    return "Unable to extract data from the file";
  }
};

/*!
 * \class Image
 * \brief Modèle représentant une image
 */
class Image : public QImage
{

protected:

    /*!
     * \brief Boolean permettant de savoir si l'image a été modifiée
     */
    bool modified = false;

public:

    /*!
     * \brief Constructeur par défaut de Image
     * \param path Pointeur vers la chaine représentant le chemin vers l'image
     */
    Image(std::string* path);

    /*!
     * \brief Constructeur par défaut de Image
     * \param path Chaine représentant le chemin vers le fichier
     */
    Image(std::string path) : Image(&path){}

    /*!
     * \brief Create a new image from a given image
     * \param image
     */
    Image(const QImage & image) : QImage(image){}

    /*!
     * \brief Applique un filtre donné à l'image
     * \param f Filtre à appliquer
     */
    void applyFilter(const Filter *f);

    /*!
     * \brief Enregistre l'image vers un chemin donné
     * \param path Chemin vers lequel enregistrer l'image
     * \return true si l'image a bien été enregistrée, false sinon
     */
    bool save(const char* path) const;

};




#endif // IMAGE_H
