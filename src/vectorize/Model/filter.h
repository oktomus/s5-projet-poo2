#ifndef FILTER_H
#define FILTER_H

/*!
  * \file filter.h
  * \brief Filtre d'image
  * \author Kevin.M
  * \version 0.1
  */


class Image;

/*!
 * \class Filter
 * \brief Filtre permettant de modifier une image
 */
class Filter
{
public:
    Filter(){}

    /*!
     * \brief Compute the filter on a given image
     * \param img A pointer to the image to compute
     */
    virtual void compute(Image * img) const = 0;

};




#endif // FILTER_H
