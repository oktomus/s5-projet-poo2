#ifndef SOBEL_H
#define SOBEL_H

/*!
  * \file sobel.h
  * \brief Filtre de Sobel
  * \author Kevin.M
  * \version 0.1
  */

#include "filter.h"


/*!
 * \class Sobel
 * \brief Filtre de Sobel
 */
class Sobel : public Filter
{
public:

    /*!
     * \brief Constructeur par défaut
     */
    Sobel() : Filter(),
            matrice_x {
                {-1,    -2,     -1},
                {0,     0,      0},
                {1,     2,      1}
            },
            matrice_y {
                {-1,    0,      1},
                {-2,    0,      2},
                {-1,    0,      1}
            }
    {}


    void compute(Image * img) const;

protected:

    double matrice_x[3][3];
    double matrice_y[3][3];

};






#endif // SOBEL_H
