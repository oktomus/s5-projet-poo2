#ifndef FASTBLUR_H
#define FASTBLUR_H

/*!
  * \file fastblur.h
  * \brief Filtre de flou rapide
  * \author Kevin.M
  * \version 0.1
  */

#include "filter.h"


/*!
 * \class FastBlur
 * \brief Filtre de flou rapide
 */
class FastBlur : public Filter
{
public:

    /*!
     * \brief Taille du flou du filtre
     */
    int size;

    /*!
     * \brief Constructeur par défaut
     * \param size Pointeur vers la taille du flou
     */
    FastBlur(int *size) : Filter(){
        this->size = *size;
    }

    FastBlur(int size) : FastBlur(&size){}

    void compute(Image * img) const;

    void setSize(int v){
        this->size = v;
    }


};


#endif // FASTBLUR_H
