#ifndef INTERACTIVETHRESHOLDVIEW_H
#define INTERACTIVETHRESHOLDVIEW_H


#include "interactiveeffectview.h"
#include "Model/threshold.h"
#include <QSlider>
#include <QVBoxLayout>
#include <QtWidgets>

class InteractiveThresholdView : public InteractiveEffectView
{
public:
    InteractiveThresholdView(ImageInterface * data_img) : InteractiveEffectView(data_img){
        this->filtre = new Threshold(50);

        this->threshold = new QSlider(Qt::Horizontal);
        QLabel *name = new QLabel("Threshold");
        this->settings_layout->addWidget(name);
        this->settings_layout->addWidget(this->threshold);
        this->threshold->setMinimum(0);
        this->threshold->setMaximum(100);

        connect(this->threshold, SIGNAL(valueChanged(int)), this, SLOT(previewModification()));
    }

private:
    QSlider * threshold;

protected:
    void updateFilter(){
        ((Threshold *) this->filtre)->setValue(this->threshold->value());
    }
};


#endif // INTERACTIVETHRESHOLDVIEW_H
